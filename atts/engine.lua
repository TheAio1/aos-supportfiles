--config
SOUNDS_PATH = "./"
--code
args={...}
function cleanString(str)
    local letters={"a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p",
                    "q","r","s","t","u","v","w","x","y","z"}
    local digits={"zero","one","two","three","four","five","six","seven","eight","nine"}
    str=string.lower(str)
    out=""
    i=0
    while true do
        i=i+1
        if i > string.len(str) then break end
        if tonumber(string.sub(str,i,i)) ~= nil then
            str=string.sub(str,1,i-1)..digits[tonumber(string.sub(str,i,i))+1]..string.sub(str,i+1,string.len(str))
        end
    end
    for i=1,string.len(str) do
        for j=1,#letters do
            if string.sub(str,i,i) == letters[j] then
                out=out..letters[j]
            end
            sleep(0)
        end
        sleep(0)
    end
    return out
end
function splitToLetters(str)
    out={}
    for i=1,string.len(str) do
        out[#out+1]=string.sub(str,i,i)
        sleep(0)
    end
    return out
end
function play(file)
    otc = term.getTextColor()
    term.setTextColor(term.getBackgroundColor())
    ox,oy = term.getCursorPos()
    shell.run("speaker play "..file)
    term.setTextColor(otc)
    term.setCursorPos(ox,oy)
end
function playLetters(lets)
    for i=1,#lets do
        play(SOUNDS_PATH..lets[i]..".dfpwm")
        sleep(0)
    end
end
function say(str)
    playLetters(splitToLetters(cleanString(str)))
end
if #args == 0 then
    print("No arguments given! What should i say?")
    say(read())
else
    say(table.concat(args))
end